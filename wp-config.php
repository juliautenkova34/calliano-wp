<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'calliano');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y|t2OOEG;@X8iK>,XGx1Zuvz?UOd%[[.z5~4LGA|p10/;-YlLY (=ns/-Lv1iJ|O');
define('SECURE_AUTH_KEY',  '&L(u[uMO3](dif>(#{rT2v!Sn=WD#Q^k/z!7<t^qizr}}zT~$CK!!H!2i=OZ X^,');
define('LOGGED_IN_KEY',    'I;>N%NVtL>ZlH6a.)rs#ex]kmfy`)KX)yVT.YVbP4!qe*HhfScq:qV tj~$FG#k[');
define('NONCE_KEY',        'r}CmG.*xS(%.M%l|;ItgO>_8cA(?`:TWYc$ >hWL|DDdig:E)MZ62S C=vY|[zEs');
define('AUTH_SALT',        'N(<J+=cJ ]$Jz.p#QfQz11S~kM^KJ?PDZZkWeEbeCF87PuL_}hw(f6157Xk/yr^.');
define('SECURE_AUTH_SALT', '/5|/_{zxHWMQY}6>P$zi|gpbO!sLJMX{itI5NqU%m:!kBprHB/)*UV34=re&M}M6');
define('LOGGED_IN_SALT',   '>XC(r.-=-J&wnapC4eVbq lX-oqTXd@MH#XB.uqn<hVx=jmz&^{-7vucd>/nNg`,');
define('NONCE_SALT',       'TV9G:Sj{AVE{kG7.&xN^:;EDzqR5W+BJ`F#y]?IUe)%IT3!36GAecUYcxi~e=  S');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
