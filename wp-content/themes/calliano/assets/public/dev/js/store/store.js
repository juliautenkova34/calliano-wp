import Vue from 'vue';
import Vuex from 'vuex';

import actions from './actions';
import mutations from './mutations';
import getters from './getters';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        languages: [
            'ru',
            'lv',
            'en'
        ],

        allMenus: {},
        cachedMenus: [],
        currentMenu: {},

        homePosts: {},
        homeContent: '',
        homeTitle: '',

        contacts: {},
        contactsContent: '',
        contactsTitle: '',

        menuPosts: {},
        menuContent: '',
        menuTitle: '',

        gallery: {}
    },
    getters,
    mutations,
    actions
});
