import Vue from 'vue';
import _ from 'lodash';

export default {
    homePosts: state => {
        return state.homePosts;
    },

    menuPosts: state => {
        return state.menuPosts;
    },

    contacts: state => {
        return state.contacts;
    },

    allMenus: state => {
        return state.allMenus;
    },

    cachedMenus: state => {
        return state.cachedMenus;
    }
}
