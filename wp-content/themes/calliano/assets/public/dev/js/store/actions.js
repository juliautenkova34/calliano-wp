import Vue from 'vue';
import _ from 'lodash';
import router from '../app.js'

export default {

    getNavigationMenus (context, payload) {
        let API = `/wp-json/wp-api-menus/v2/menus`;

        return Vue.http.get(API).then((response) => {
            context.commit('GET_NAV_MENU', response.body);
            return response.body;
        });
    },

    cacheMenus (context, payload) {

        _.each( payload, function(value, key) {
            let API = `/wp-json/wp-api-menus/v2/menus`;

            return Vue.http.get(`${API}/${value.ID}`).then((response) => {
                context.commit('CACHE_NAVIGATION_MENUS', response.body);

                let currentMenuLang = value.slug.slice(-2);

                /* GET INITIAL VALUE */
                /* IT's HARDCODED */
                if ( currentMenuLang.indexOf(router.currentRoute.params.lang || 'lv') !== -1 ) {
                    context.commit('GET_CURRENT_MENU', response.body);
                }

                return response.body;
            })
        });

    },

    getCurrentMenu (context, payload) {

        _.each( payload.data, function(value, key) {
            let currentMenuLang = value.slug.slice(-2);

            if ( currentMenuLang.indexOf(payload.lang) !== -1 ) {
                context.commit('GET_CURRENT_MENU', value);
            }
        });

    },

    getHomePosts (context, payload) {
        let API = `/wp-json/wp/v2/homepage`;

        return Vue.http.get(API).then((response) => {
            context.commit('GET_HOME_POSTS', response.body);
            return response.body;
        });
    },

    getHomeTitle (context, payload) {
        _.each(payload.data, function(value, key){
            if ( value.metadata.lang !== undefined ) {
                if ( value.metadata.lang[0] === payload.lang ) {
                    context.commit('GET_HOME_TITLE', value.title.rendered)
                }
            }
        });
    },

    getHomeContent (context, payload) {
        _.each(payload.data, function(value, key){
            if ( value.metadata.lang !== undefined ) {
                if ( value.metadata.lang[0] === payload.lang ) {
                    context.commit('GET_HOME_CONTENT', value.content.rendered)
                }
            }
        });
    },

    getMenuPosts (context, payload) {
        let API = `/wp-json/wp/v2/menu`;

        return Vue.http.get(API).then((response) => {
            context.commit('GET_MENU', response.body);
            return response.body;
        });
    },

    getMenuTitle (context, payload) {
        _.each(payload.data, function(value, key){
            if ( value.metadata.lang !== undefined ) {
                if ( value.metadata.lang[0] === payload.lang ) {
                    context.commit('GET_MENU_TITLE', value.title.rendered)
                }
            }
        });
    },

    getMenuContent (context, payload) {
        _.each(payload.data, function(value, key){
            if ( value.metadata.lang !== undefined ) {
                if ( value.metadata.lang[0] === payload.lang ) {
                    context.commit('GET_MENU_CONTENT', value.content.rendered)
                }
            }
        });
    },

    getContacts (context, payload) {

        let API = `/wp-json/wp/v2/contacts`;

        return Vue.http.get(API).then((response) => {
            context.commit('GET_CONTACTS', response.body);
            return response.body;
        });

    },

    getContactsTitle (context, payload) {
        _.each(payload.data, function(value, key){
            if ( value.metadata.lang !== undefined ) {
                if ( value.metadata.lang[0] === payload.lang ) {
                    context.commit('GET_CONTACTS_TITLE', value.title.rendered)
                }
            }
        });
    },

    getContactsContent (context, payload) {
        _.each(payload.data, function(value, key){
            if ( value.metadata.lang !== undefined ) {
                if ( value.metadata.lang[0] === payload.lang ) {
                    context.commit('GET_CONTACTS_CONTENT', value.content.rendered)
                }
            }
        });
    },

    getGallery (context, payload) {
        let homepageAPI = `/wp-json/wp/v2/gallery`;
        Vue.http.get(homepageAPI).then(function(response) {
            let data = response.body;
            context.commit('GET_GALLERY', data)
        });
    }
}
