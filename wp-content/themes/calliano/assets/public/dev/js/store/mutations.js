export default {

    // NAVIGATION MENU

    GET_NAV_MENU (state, payload) {
        state.allMenus = payload;
    },

    GET_CURRENT_MENU (state, payload) {
        state.currentMenu = payload;
    },

    CACHE_NAVIGATION_MENUS (state, payload) {
        state.cachedMenus.push(payload);
    },

    // HOME

    GET_HOME_POSTS (state, payload) {
        state.homePosts = payload;
    },

    GET_HOME_TITLE (state, payload) {
        state.homeTitle = payload;
    },

    GET_HOME_CONTENT (state, payload) {
        state.homeContent = payload;
    },

    // CONTACTS

    GET_CONTACTS (state, payload) {
        state.contacts = payload;
    },

    GET_CONTACTS_TITLE (state, payload) {
        state.contactsTitle = payload;
    },

    GET_CONTACTS_CONTENT (state, payload) {
        state.contactsContent = payload;
    },

    // MENU

    GET_MENU (state, payload) {
        state.menuPosts = payload;
    },

    GET_MENU_TITLE (state, payload) {
        state.menuTitle = payload;
    },

    GET_MENU_CONTENT (state, payload) {
        state.menuContent = payload;
    },

    // GALLERY

    GET_GALLERY (state, payload) {
        state.gallery = payload;
    }
};
