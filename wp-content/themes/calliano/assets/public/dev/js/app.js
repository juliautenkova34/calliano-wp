import '../scss/main.scss';

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

import { store } from './store/store';

import App from './App.vue';

import Home from './Home.vue';
import Menu from './Menu.vue';
import Gallery from './Gallery.vue';
import Contacts from './Contacts.vue';


Vue.use(VueRouter);
Vue.use(VueResource);

const routes = [
    { path: '/:lang/menu', name: 'menu', component: Menu },
    { path: '/:lang/gallery', name: 'gallery', component: Gallery },
    { path: '/:lang/contacts', name: 'contacts', component: Contacts },
    { path: '/:lang', name: 'home', component: Home },
    { path: '*', redirect: '/lv/' }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

const vm = new Vue({
    el: '#CALLIANO',
    router,
    store,
    render: h => h(App)
});

export default router;
