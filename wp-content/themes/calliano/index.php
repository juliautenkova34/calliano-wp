<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<!--        <link rel="shortcut icon" href="--><?php //echo esc_url( get_template_directory_uri() ); ?><!--/assets/images/favicon.ico" type="image/x-icon" />-->
<!--        <link rel="icon" href="--><?php //echo esc_url( get_template_directory_uri() ); ?><!--/assets/images/favicon.ico" type="image/x-icon" />-->
<!---->
<!--        <link rel="stylesheet" href="--><?php //echo esc_url( get_template_directory_uri() ); ?><!--/assets/bootstrap/css/bootstrap.min.css">-->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/public/build/main.post.css">
    <?php wp_head(); ?>
</head>

<body>

    <script>
        var URL = '<?php echo esc_url( get_template_directory_uri() ); ?>';
    </script>

    <div id="CALLIANO"></div>

    <div class="hidden" style="display: none">
        <?php echo do_shortcode('[cf7-form cf7key="contact-form-ru_copy"]') ?>
        <?php echo do_shortcode('[cf7-form cf7key="contact-form-en_copy"]') ?>
        <?php echo do_shortcode('[cf7-form cf7key="contact-form-1"]]') ?>
    </div>


    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/public/build/app.dist.js"></script>

    <?php wp_footer(); ?>
</body>

</html>