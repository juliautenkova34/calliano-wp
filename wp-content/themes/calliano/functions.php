<?php

// Disabling top admin bar
add_filter('show_admin_bar', '__return_false');

add_theme_support('post-formats', []);



// Removing / Deleting "Trash" button from admin
// panel, so stupid people couldn't accidentaly
// delete important posts

//function remove_row_actions( $actions )
//{
//    if( get_post_type() === 'post' )
//
//        unset( $actions['trash'] );
//
//    return $actions;
//}

add_action( 'init', 'register_my_menus' );

function register_my_menus() {
    register_nav_menus(
        array(
            'primary' => __( 'Main Navigation Menu' )
        )
    );
}


add_theme_support( 'post-thumbnails' );

// Our custom post type function
function create_posttype() {
    register_post_type( 'homepage',
        array(
            'labels' => array(
                'name' => __( 'Homepage' ),
                'singular_name' => __( 'Homepage' )
            ),
            'public' => true,
            'show_in_rest' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'homepage'),
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields' )
        )
    );

    register_post_type( 'menu',
        array(
            'labels' => array(
                'name' => __( 'Menu' ),
                'singular_name' => __( 'Menu' )
            ),
            'public' => true,
            'show_in_rest' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'menu'),
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields' )
        )
    );

    register_post_type( 'contacts',
        array(
            'labels' => array(
                'name' => __( 'Contacts' ),
                'singular_name' => __( 'Contacts' )
            ),
            'public' => true,
            'show_in_rest' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'contacts'),
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields' )
        )
    );

    register_post_type( 'gallery',
        array(
            'labels' => array(
                'name' => __( 'Gallery' ),
                'singular_name' => __( 'Gallery' )
            ),
            'public' => true,
            'show_in_rest' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'gallery'),
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields' )
        )
    );
}

// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );
//
register_rest_field( 'homepage', 'metadata', array(
    'get_callback' => function ( $data ) {
        return get_post_meta( $data['id'], '', '' );
    }, ));

register_rest_field( 'menu', 'metadata', array(
    'get_callback' => function ( $data ) {
        return get_post_meta( $data['id'], '', '' );
    }, ));

register_rest_field( 'contacts', 'metadata', array(
    'get_callback' => function ( $data ) {
        return get_post_meta( $data['id'], '', '' );
    }, ));

register_rest_field( 'gallery', 'metadata', array(
    'get_callback' => function ( $data ) {
        return get_post_meta( $data['id'], '', '' );
    }, ));
////



add_action( 'rest_api_init', 'add_thumbnail_to_JSON' );

function add_thumbnail_to_JSON() {
//Add featured image
    register_rest_field(
        '$data', // Where to add the field (Here, blog posts. Could be an array)
        'featured_image_src', // Name of new field (You can call this anything)
        array(
            'get_callback'    => 'get_image_src',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_image_src( $object, $field_name, $request ) {
    $feat_img_array = wp_get_attachment_image_src(
        $object['featured_media'], // Image attachment ID
        'full',  // Size.  Ex. "thumbnail", "large", "full", etc..
        true // Whether the image should be treated as an icon.
    );
    return $feat_img_array[0];
}


?>